from django.db import models
from django.conf import settings

# Create your models here.

class Recipe(models.Model):
    title = models.CharField(max_length=200, verbose_name="Name")
    picture = models.URLField(verbose_name="Image URL")
    rating = models.CharField(max_length=200, null=True, verbose_name="Rating out of 5.0")
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.title

class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe,
        related_name="steps",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['step_number']

class RecipeIngredient(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe,
        related_name="ingredients",
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return self.title

    class Meta:
        ordering = ['food_item']
