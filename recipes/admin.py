from django.contrib import admin
from recipes.models import Recipe, RecipeStep, RecipeIngredient

# Register your models here.
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
        "description"
    )

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "step_number",
        "id",
        "instruction",
    )

@admin.register(RecipeIngredient)
class RecipeIngredient(admin.ModelAdmin):
    list_display = (
        "food_item",
        "amount",
        "id"
    )
